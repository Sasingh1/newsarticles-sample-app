//
//  ArticleDataTests.swift
//  NewsArticlesTests
//
//  Created by Sartaj Singh on 7/5/19.
//  Copyright © 2019 Sartaj Singh. All rights reserved.
//

import XCTest

@testable import NewsArticles

let kSampleArticleHeadline = "China cuts through Trump turmoil"
let kSampleArticleAbstract =  "Volatility in financial markets triggered by US President Donald Trump's latest tweets provide no insight into the performance of China's economy."
let kSampleArticleByLine = "Tony Boyd"

class ArticleDataTests: XCTestCase {

    var service : WebServiceHandler!
    
    override func setUp() {
        super.setUp()
        service = WebServiceHandler.sharedInstance
    }
    override func tearDown() {
        self.service = nil
        super.tearDown()
    }
    
    func testParseEmptyArticleData() {
        //giving the empty data
        let data = [String: AnyObject]()
        
        //giving completion after parsing
        // expected valid NewsData with valid Article data
        let completion : ((Result<[Article], ErrorResult>) -> Void) = { result in
            switch result {
            case .success(_):
                XCTAssert(false, "Expected failure when no data")
            default:
                break
            }
        }
        service.parseJson(data: data, completion: completion)
    }
    
    func testParseArticleData() {
        
        // giving a sample json file
        guard let data = FileManager.readJson(forResource: "articles") else {
            XCTAssert(false, "Can't get data from articles")
            return
        }
        
        // giving completion after parsing
        // expected valid converter with valid data
        let completion : ((Result<[Article], ErrorResult>) -> Void) = { result in
            switch result {
            case .failure(_):
                XCTAssert(false, "Expected valid newsData")
            case .success(let newsData):
                XCTAssertEqual(newsData.first?.headline, kSampleArticleHeadline, "Expected \(kSampleArticleHeadline)")
                XCTAssertEqual(newsData.first?.abstractInfo, kSampleArticleAbstract, "Expected \(kSampleArticleAbstract)")
                XCTAssertEqual(newsData.first?.byLine, kSampleArticleByLine, "Expected \(kSampleArticleByLine)")
            }
        }
        service.parseJson(data: data, completion: completion)
    }
}


extension FileManager {
    
    static func readJson(forResource fileName: String) -> [String: AnyObject]? {
        let bundle = Bundle(for: ArticleDataTests.self)
        if let path = bundle.path(forResource: fileName, ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: . mappedIfSafe)
                let list:  [String: AnyObject]? = try? JSONSerialization.jsonObject(with: data, options: []) as?  [String: AnyObject]
                return list
            } catch {
                //handle error
            }
        }
        return nil
    }
}
