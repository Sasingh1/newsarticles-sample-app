//
//  ArticlesDataSourceTest.swift
//  NewsArticlesTests
//
//  Created by Sartaj Singh on 7/5/19.
//  Copyright © 2019 Sartaj Singh. All rights reserved.
//

import XCTest
@testable import NewsArticles
class ArticlesDataSourceTest: XCTestCase {
    
    var articleTableViewController: ArticlesHomeTableViewController!
    
    override func setUp() {
        super.setUp()
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        self.articleTableViewController = storyboard.instantiateViewController(withIdentifier: "ArticlesTableViewIdentifier") as? ArticlesHomeTableViewController
        
        // giving data value
        let newsData: [Article]? = [Article(json: ["headline": "Test News Article"])!]
        
        articleTableViewController.arrayArticles = newsData!
    }
    
    override func tearDown() {       
        super.tearDown()
    }
    
    func testHasATableView() {
        XCTAssertNotNil(articleTableViewController.tableView)
    }
    
    func testTableViewHasDelegate() {
        XCTAssertNotNil(articleTableViewController.tableView.delegate)
    }
    
    func testTableViewConfromsToTableViewDelegateProtocol() {
        XCTAssertTrue(articleTableViewController.conforms(to: UITableViewDelegate.self))
        XCTAssertTrue(articleTableViewController.responds(to: #selector(articleTableViewController.tableView(_:didSelectRowAt:))))
    }
    
    func testTableViewHasDataSource() {
        XCTAssertNotNil(articleTableViewController.tableView.dataSource)
    }
    
    func testTableViewConformsToTableViewDataSourceProtocol() {
        XCTAssertTrue(articleTableViewController.conforms(to: UITableViewDataSource.self))
        XCTAssertTrue(articleTableViewController.responds(to: #selector(articleTableViewController.numberOfSections(in:))))
        XCTAssertTrue(articleTableViewController.responds(to: #selector(articleTableViewController.tableView(_:numberOfRowsInSection:))))
        XCTAssertTrue(articleTableViewController.responds(to: #selector(articleTableViewController.tableView(_:cellForRowAt:))))
    }
    
    func testTableViewCellHasReuseIdentifier() {
        let cell = articleTableViewController.tableView(articleTableViewController.tableView, cellForRowAt: IndexPath(row: 0, section: 0)) as? ArticleCell
        let actualReuseIdentifer = cell?.reuseIdentifier
        let expectedReuseIdentifier = "ArticleCellIdentifier"
        
        // expected ArticleCell reusable identifier
        XCTAssertEqual(actualReuseIdentifer, expectedReuseIdentifier)
    }
    
    func testTableCellHasCorrectLabelText() {
        let cell0 = articleTableViewController.tableView(articleTableViewController.tableView, cellForRowAt: IndexPath(row: 0, section: 0)) as? ArticleCell
        
        // expected ArticleCell class label value
        XCTAssertEqual(cell0?.labelHeadline.text, "Test News Article")
    }
}
