//
//  ArticlesViewModelTests.swift
//  NewsArticlesTests
//
//  Created by Sartaj Singh on 6/5/19.
//  Copyright © 2019 Sartaj Singh. All rights reserved.
//

import XCTest
@testable import NewsArticles

class ArticleViewModelTests: XCTestCase {
    
    var viewModel : ArticleViewModel?
    fileprivate var service : MockServiceHelper!
    var viewModelWithArticles : ArticleViewModel!
    fileprivate var serviceWithArticle : MockServiceHelperWithArticle!
    
    override func setUp() {
        super.setUp()
        self.service = MockServiceHelper()
        self.viewModel = ArticleViewModel(service: service)
        self.serviceWithArticle = MockServiceHelperWithArticle()
        self.viewModelWithArticles = ArticleViewModel(service: serviceWithArticle)
        
    }
    
    override func tearDown() {
        self.viewModel = nil
        self.service = nil
        super.tearDown()
    }
    
    func testFetchWithNoService() {
        // giving no service to a view model
        self.viewModel?.service = nil
        
        // expected to not be able to fetch article
        self.viewModel?.fetchArticles { (result, articleData) in
            switch result {
            case .success :
                XCTAssert(false, "ViewModel should not be able to fetch without service")
            default:
                break
            }
        }
    }
    
    func testFetchArticles() {
        // expected completion to succeed
        viewModelWithArticles.fetchArticles { (result, articleData) in
            switch result {
            case .failure :
                XCTAssert(false, "ViewModel should not be able to fetch without service")
            default:
                break
            }
        }
    }
    
    func testFetchNoArticles() {        
        // expected completion to fail
        viewModel?.fetchArticles { (result, articleData) in
            switch result {
            case .success :
                XCTAssert(false, "ViewModel should not be able to fetch ")
            default:
                break
            }
        }
    }
    
    
}

fileprivate class MockServiceHelper : WebServiceHandler {
    var newsData: [Article]?
    override func fetchArticlesServiceCall(completion : @escaping ((Result<[Article], ErrorResult>) -> Void)) {
        if let newsData = newsData {
           completion(Result.success(newsData))
        } else {
           completion(Result.failure(ErrorResult.custom(string: "No NewsData")))
        }
    }
}

fileprivate class MockServiceHelperWithArticle : WebServiceHandler {
    var newsData: [Article]? = [Article(json: ["headline": "Test News Article"])!]
    override func fetchArticlesServiceCall(completion: @escaping ((Result<[Article], ErrorResult>) -> Void)) {
        if let newsData = newsData {
            completion(Result.success(newsData))
        } else {
            completion(Result.failure(ErrorResult.custom(string: "No NewsData")))
        }
    }
}
