
# NewsArticle Sample App

The following points have been covered in the sample news article app:
•    Show a list of articles newest first(sorted based on timestamp)
•    Smallest available image is shown as thumbnail on tableview cell 
•    Images are loaded asynchronously and cached using AlamofireImage
•    Unit Test cases are included in the solution

Code Compilation
The application is created using  Xcode 10.2.1.
Use NewsArticles.xcworkspace to open project 

App Architecture
MVVM design pattern is used to create application.

Model - Matching with the api model, two struct are created for Article and ArticleImage
ViewModel - ArticleViewModel is used to fetch articles from API and transform model information that displayed using tableView.
ViewController - The tableview is used with custom cell ‘ArticleCell’ to display Article information. WKWebView is used to display article details.
StoryBoard screens are created using Auto-layout to make it responsive

3rd party libraries used
The pod file is used using following libraries:
•    Alamofire - Alamofire is used to make API request call
•    AlamofireImage - AlamofireImage is an image component library for Alamofire and used  for In-Memory Image Cache
•    SVProgressHUD - SVProgressHUD is a clean and easy-to-use HUD meant to display the progress of an ongoing task on iOS
