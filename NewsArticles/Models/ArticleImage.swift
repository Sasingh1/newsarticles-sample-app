//
//  ArticleImage.swift
//  NewsArticles
//
//  Created by Sartaj Singh on 4/5/19.
//  Copyright © 2019 Sartaj Singh. All rights reserved.
//

import Foundation

struct ArticleImage {
    var imageUrl: String!
    var imageSize: Int!
    var width: Int!
    var height: Int!
    
    init?(json: [String: Any]?) {
        guard let jsonData = json else { return nil }
        imageUrl = jsonData["url"] as? String ?? ""
        width = jsonData["width"] as? Int ?? 0
        height = jsonData["height"] as? Int ?? 0
        //storing imagesize to find smallest image
        imageSize = width * height
    }
    
    init() {
        self.init(json: [:])!
    }
}
