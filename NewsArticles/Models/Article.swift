//
//  Article.swift
//  NewsArticles
//
//  Created by Sartaj Singh on 3/5/19.
//  Copyright © 2019 Sartaj Singh. All rights reserved.
//

import Foundation

struct Article {
    
    var headline: String!
    var abstractInfo: String!
    var byLine: String!
    var imageUrl: String!
    var relatedImages: [ArticleImage]
    var timeStamp: Double
    var articleDetailUrl: String!
    
    
    init?(json: [String: Any]?) {
        guard let jsonData = json else { return nil }
        headline = jsonData["headline"] as? String ?? ""
        abstractInfo = jsonData["theAbstract"] as? String ?? ""
        byLine = jsonData["byLine"] as? String ?? ""
        relatedImages = (jsonData["relatedImages"] as? [[String: Any]] ?? []).compactMap{ArticleImage(json: $0)}        
        // smallest image available based on size
        let smallestArticleImage = relatedImages.reduce(relatedImages.first, { $0!.imageSize < $1.imageSize ? $0 : $1 })
        imageUrl = smallestArticleImage?.imageUrl
        timeStamp = jsonData["timeStamp"] as? Double ?? 0.0
        articleDetailUrl = jsonData["url"] as? String ?? ""
    }
}
