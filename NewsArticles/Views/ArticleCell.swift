//
//  ArticleCell.swift
//  NewsArticles
//
//  Created by Sartaj Singh on 4/5/19.
//  Copyright © 2019 Sartaj Singh. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class ArticleCell: UITableViewCell {

    @IBOutlet weak var labelHeadline: UILabel!
    @IBOutlet weak var labelAbstract: UILabel!
    @IBOutlet weak var labelByLine: UILabel!
    @IBOutlet weak var articleImage: UIImageView!
    
    var articleModel: Article? {
        didSet {
            guard let data = articleModel else {return}
            labelHeadline.text = data.headline!
            labelAbstract.text = data.abstractInfo!
            labelByLine.text = data.byLine!            
            loadImage(with: data.imageUrl)
        }
        
        
    }
    func loadImage(with urlStr: String?) {
        let imageCache = AutoPurgingImageCache(
            memoryCapacity: 100_000_000,
            preferredMemoryUsageAfterPurge: 60_000_000
        )
        guard let urlStr = urlStr else { return }
        if let image = imageCache.image(withIdentifier: urlStr) {
            populate(with: image)
        }else{
            Alamofire.request(urlStr).responseImage { response in
                if response.result.value != nil {
                    let image = UIImage(data: response.data!, scale: 1.0)!
                    self.populate(with: image)
                    imageCache.add(image, withIdentifier: urlStr)
                }
            }
            
        }
    }
    
    func populate(with image: UIImage) {
        articleImage.image = image
    }
}
