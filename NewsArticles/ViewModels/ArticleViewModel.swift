//
//  ArticleViewModel.swift
//  NewsArticles
//
//  Created by Sartaj Singh on 6/5/19.
//  Copyright © 2019 Sartaj Singh. All rights reserved.
//

import Foundation
import SVProgressHUD

struct ArticleViewModel {
    
    weak var service: WebServiceHandler?
    enum result: String {
        case success
        case failure
        
    }
    
    init(service: WebServiceHandler = WebServiceHandler.sharedInstance) {
        self.service = service
    }
    
    func fetchArticles(_ completionHandler: @escaping (_ finished: result, _ articleData: [Article]) -> Void) {
        SVProgressHUD.show()
        guard let service = service else {
            completionHandler(.failure, [])
            return
        }
        service.fetchArticlesServiceCall { result in
            switch result {
            case .success(let arrayArticles) :
                var arrayArticles: [Article] = arrayArticles
                
                // Sort Articles on the bases of timestamp
                arrayArticles = arrayArticles.sorted(by: { $0.timeStamp > $1.timeStamp })
                completionHandler(.success, arrayArticles)
                break
            case .failure( _) :
                completionHandler(.failure, [])
                break
        }            
        SVProgressHUD.dismiss()
    }
    
}
}
