//
//  ArticleDetailViewController.swift
//  NewsArticles
//
//  Created by Sartaj Singh on 5/5/19.
//  Copyright © 2019 Sartaj Singh. All rights reserved.
//

import UIKit
import WebKit
import SVProgressHUD

class ArticleDetailViewController: UIViewController {

    @IBOutlet weak var webView: WKWebView!
    
    var articleDetailUrl: String?
    override func viewDidLoad() {
        super.viewDidLoad()
        webView.navigationDelegate = self
        sendRequest(urlString: articleDetailUrl!)
    }
    
    // Convert String into URL and load the URL
    private func sendRequest(urlString: String) {
        let myURL = URL(string: urlString)
        let myRequest = URLRequest(url: myURL!)
        webView.allowsBackForwardNavigationGestures = true
        webView.load(myRequest)
    }
    override func viewWillDisappear(_ animated: Bool) {
        if self.webView.isLoading { SVProgressHUD.dismiss() }
    }
}

extension ArticleDetailViewController: WKNavigationDelegate {
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        SVProgressHUD.dismiss()
    }
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        // Set the indicator everytime webView started loading
        SVProgressHUD.show()
    }
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        SVProgressHUD.dismiss()
    }  
}
