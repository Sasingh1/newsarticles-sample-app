//
//  ArticlesHomeTableViewController.swift
//  NewsArticles
//
//  Created by Sartaj Singh on 4/5/19.
//  Copyright © 2019 Sartaj Singh. All rights reserved.
//

import UIKit

let kEstimatedNewsRableRowHeight:CGFloat = 100.0

class ArticlesHomeTableViewController: UITableViewController {

    //MARK:- Segues
    enum Segues: String {
        case showDetail = "toDetailViewController"
    }
    
    var arrayArticles: [Article] = []
    var articleViewModel: ArticleViewModel = ArticleViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUI()
        setupDataSource()
    }
    
    //MARK:- UI setu methods
    func setUpUI() {
        self.title = "News"
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.estimatedRowHeight = kEstimatedNewsRableRowHeight
    }
    
    func setupDataSource() {
        // Fetch articles
        self.articleViewModel.fetchArticles { (finished, response) in
            DispatchQueue.main.async {
                self.arrayArticles = response
                self.tableView.reloadData()
            }
        }
    }

    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrayArticles.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ArticleCellIdentifier", for: indexPath) as! ArticleCell
        cell.articleModel = arrayArticles[indexPath.row]
        return cell
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 1.0
    }
    
    // MARK: - Table view delegate
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "toDetailViewController", sender: indexPath)
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch Segues(rawValue: segue.identifier!) {
        case .showDetail?:
            let indexPath = sender as! IndexPath
            if let controller = segue.destination as? ArticleDetailViewController {
                let selectedArticle: Article = arrayArticles[indexPath.row]
                controller.articleDetailUrl = selectedArticle.articleDetailUrl
            }
        default:
            break
        }
    }
 

}
