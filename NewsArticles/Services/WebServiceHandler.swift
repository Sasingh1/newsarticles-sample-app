//
//  WebServiceHandler.swift
//  NewsArticles
//
//  Created by Sartaj Singh on 3/5/19.
//  Copyright © 2019 Sartaj Singh. All rights reserved.
//

import Foundation
import Alamofire

//MARK:- Service enums
enum Result<T, E: Error> {
    case success(T)
    case failure(E)
}
enum ErrorResult: Error {
    case network(string: String)
    case parser(string: String)
    case custom(string: String)
}

class WebServiceHandler {
    static let sharedInstance = WebServiceHandler()
    var arrayArticles: [Article] = []
    
    
    func apiRequest(method: Alamofire.HTTPMethod, url: URL, CompletionHandler: @escaping (_ finished: Bool, _ response: AnyObject?) -> Void) {
        Alamofire.request(url, method: method).responseJSON { response in
            switch response.result {
            case .success(let value):
                CompletionHandler(true, value as AnyObject)
            case .failure(let error):
                CompletionHandler(false, error as AnyObject)
                }
            }
    }
    
    func fetchArticlesServiceCall(completion: @escaping ((Result<[Article], ErrorResult>) -> Void)) {
        self.apiRequest(method: .get, url: APIManager.newsArticlesAPIURL()) {
            (finished, response) in
            DispatchQueue.main.async {
                if(finished) {
                    print("Response")
                    print(response as? [String: AnyObject] ?? "")
                    guard let json = response as? [String: AnyObject] else {return}
                    self.parseJson(data: json, completion: completion)
                } else {
                    completion(Result.failure(ErrorResult.custom(string: "No Data Available")))
                }
            }
        }
    }
    func parseJson(data: [String: AnyObject], completion : @escaping (Result<[Article], ErrorResult>) -> Void) {
        guard let list = data["assets"] as? [AnyObject] else { return completion(Result.failure(ErrorResult.custom(string: "No Data Available"))) }
        var arrayArticle: [Article] = []
        for value in list {
            let result = Article.init(json: value as? [String: AnyObject])
            arrayArticle.append(result!)
        }
        completion(Result.success(arrayArticle))
    }
}
