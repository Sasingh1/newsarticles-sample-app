//
//  APIManager.swift
//  NewsArticles
//
//  Created by Sartaj Singh on 3/5/19.
//  Copyright © 2019 Sartaj Singh. All rights reserved.
//

import Foundation

struct APIManager {
    private static let baseUrlString = "https://bruce-v2-mob.fairfaxmedia.com.au/1/coding_test/13ZZQX/full"
    
    public static func newsArticlesAPIURL() -> URL {
        let url = URL(string: baseUrlString)!
        return url
    }
}
